#include <iostream> 
using namespace std;

void scalanie(int array[], int p, int q, int r){
    int n1 = q - p + 1;
    int n2 = r - q;
 
    int * L = new int[n1];
    int * R = new int[n2];

    for (int i = 0; i < n1; i++)
        L[i] = array[p + i];
    for (int j = 0; j < n2; j++)
        R[j] = array[q + 1 + j];

    int i = 0;
    int j = 0;
    int k = p;

    while (i < n1 && j < n2){
        if (L[i] <= R[j]){
            array[k] = L[i];
            i++;
        }
        else{
            array[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1){
        array[k] = L[i];
        i++;
        k++;
    }
 
    while (j < n2){
        array[k] = R[j];
        j++;
        k++;
    }
    delete [] L;
    delete [] R;
}

void sortowanie_przez_scalanie(int array[], int p, int r){
    if (p < r){
        int q = p + (r - p) / 2;

        sortowanie_przez_scalanie(array, p, q);
        sortowanie_przez_scalanie(array, q + 1, r);

        scalanie(array, p, q, r);
    }
}

 
void wypis(int array[], int size)
{
    for (int i = 0; i < size; i++)
        cout << array[i] << " ";
}

int main()
{
    int arr[] = { 12, 11, 13, 5, 6, 7 };
    int arr_size = sizeof(arr) / sizeof(arr[0]);
    sortowanie_przez_scalanie(arr, 0, arr_size - 1);
    cout << "tablica 1 \n";
    wypis(arr, arr_size);

    int arr2[] = { 12, 11, 13, 5, 6, 7, 2, -2, 20, -11, -5 };
    int arr2_size = sizeof(arr2) / sizeof(arr2[0]);
    sortowanie_przez_scalanie(arr2, 0, arr2_size - 1);
    cout << "\ntablica 2 \n";
    wypis(arr2, arr2_size);




    return 0;
}